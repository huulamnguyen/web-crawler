import React from "react";
import {render} from 'react-dom';
import BasicTable from './basic-table'
class Hello extends React.Component {
    render() {
        return (
            <React.Fragment>
            <h1>COMPANIES</h1>
            <BasicTable/>
            </React.Fragment>
        );
    }
}

// Render class Hello ở trên vào vị trí có id là root đã được khai báo trong file index.html
render(<Hello />, document.getElementById("root"));