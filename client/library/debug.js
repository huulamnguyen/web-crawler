"use strict";
require('dotenv');
const env = process.env.NODE_ENV;

module.exports = function (...args) {
  if (env !== "production") {
    console.log.apply(console, args);
  }
}