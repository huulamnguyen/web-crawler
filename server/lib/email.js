"use strict";
var nodemailer = require('nodemailer');
var logger = require('./logger');

module.exports.sendMail = function ({ from, to, subject, body }) {
  return new Promise((resolve, reject) => {
    if (!to) {
      reject(new Error('invalid mail receiver'));
      return;
    }
    if (!subject) {
      reject(new Error('invalid mail subject'));
      return;
    }
    if (!body) {
      reject(new Error('invalid mail content'));
      return;
    }
    var mailHostConfig = {
      host: 'mail.gcalls.co',
      port: 587,
      auth: {
        user: 'mailservice@gcalls.co',
        pass: 'mailservice@Gcall2016,.'
      }
    };
    var smtp = nodemailer.createTransport(mailHostConfig);
    var mailOption = {
      from: !!from ? from : "Gcalls Company <mailservice@gcalls.co>",
      to,
      subject,
      generateTextFromHTML: true,
      html: body
    };
    smtp.sendMail(mailOption, (error, response) => {
      if (error) {
        logger.error(error.message);
        reject(error);
      } else {
        resolve(response);
      }
    });
    smtp.close();
  });
};

module.exports.validateEmail = function (email) {
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};