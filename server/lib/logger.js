"use strict";
var winston = require('winston');
var fs = require('fs');
var rfs = require('rotating-file-stream');
var logDir = 'logs';

/**
 * check if folder exists or not
 */
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

/**
 * logger setup
 */
var logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      filename: logDir + '/all-logs.log',
      timestamp: new Date(),
      handleExceptions: true,
      json: true,
      colorize: false
    }),
    new winston.transports.Console({
      handleExceptions: true,
      timestamp: new Date(),
      colorize: true
    })
  ],
  exitOnError: false
});

module.exports = logger;

var logStream = rfs('morgan-' + new Date().toISOString() +'.log', {
  interval: '7d',
  path: logDir
});

module.exports.morganLog = logStream;