const axios = require('axios');
const https = require('https');
const { domain, api_key } = require('../config/feature');

module.exports = axios.create({
	baseURL: domain,
	headers: { 'APIKey': api_key }
});
