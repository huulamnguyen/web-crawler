"use strict";

module.exports.validatePhone = function(phone) {
  let pattern = /^[0-9]{1,12}$/;
  return pattern.test(phone);
}