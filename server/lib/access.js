const axios = require('axios');
const https = require('https');
const config = require('../config/access');
const token = "Bearer " + new Buffer(config.username + ":" + config.password).toString("base64");

var instance = axios.create({
	baseURL: config.host,
	headers: { "Authorization": token },
	httpsAgent: new https.Agent({ rejectUnauthorized: false })
});

module.exports = instance;
