"use strict";
module.exports = {
  isLoggedIn: function(req, res, next) {
    if (req.session.user) {
      next();
    } else {
      res.status(401).json({
        success: false,
        error: {
          message: 'invalid session'
        }
      });
    }
  },
  isNotLoggedIn: function (req, res, next) {
    if (!req.session.user) {
      next();
    } else {
      res.status(402).json({
        success: false,
        error: {
          message: 'invalid access'
        }
      });
    }
  },
  //Edited
  isAdmin: function(req, res, next) {
    if (req.session.user && req.session.user.role === 'admin') {
      next();
    } else {
      res.status(401).json({
        success: false,
        error: {
          message: 'invalid session'
        }
      });
    }
  },
  isAdminPage: function (req, res, next) {
    if (req.session.user) {
      next();
    } else {
      res.redirect('/');
    }
  }
};