import config from '../config/authorization';

module.exports = (req, res, next) => {
	console.log("authorization")
	if (req.session.user) { return next(); }
	if (req.headers.authorization) {
		const base64code = req.headers.authorization.split(' ')[1];
		const [username, password] = new Buffer(base64code, 'base64').toString().split(':');
		//console.log("base64code",username, password)
		return (username === config.username && password === config.password) ?
			next() : res.status(403).send({ success: false, error: { message: 'invalid authorization' } });
	}
	return res.status(403).send({ success: false, error: { message: 'no authorization' } });
};
