require('dotenv').config();

module.exports = {
	domain: process.env.DASHBOARD_DOMAIN,
	api_key: process.env.DASHBOARD_API_KEY
};
