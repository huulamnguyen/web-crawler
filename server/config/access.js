require('dotenv').config();

module.exports = {
	host: process.env.ACCESS_SERVICE_HOST,
	username: process.env.ACCESS_SERVICE_USERNAME,
	password: process.env.ACCESS_SERVICE_PASSWORD
};
