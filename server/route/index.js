"use strict";
var path = require('path');
var companies = require('./companies');
var request = require("request");

module.exports = (app) => {

	app.use('/api', companies);

	app.route('/')
		.get((req, res) => {
			res.sendFile(path.resolve(__dirname + '/../views/agent.html'));
		});

};
