"use strict";
var jwt = require('jsonwebtoken');
var privateKey = '3VeG3vNqcKHwwa2L1\nM7wGds72mvuIsEHP\nzoAKVx3G4546hvbUH\nuE3WB7Yxdfst0anxht\nmB1hUFsqX73a0Zugc\nCWirR8Xniiht2WNUg\nwEKCifbdsYghz8fztd\nVFIEmsOUfdstrFOlK\nx4Ri6dXfpXh1LzHjw\nGABexGFD5Hzyu0r'

module.exports.encode = function(data) {
  return jwt.sign(data, privateKey);
}

module.exports.decode = function(token) {
  return jwt.verify(token, privateKey);
}