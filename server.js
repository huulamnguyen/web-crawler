"use strict";
var http = require('http');
var express = require('express');
// var morgan = require('morgan');
var webpack = require('webpack');
var session = require('express-session');
var bodyParser = require('body-parser');
var cors = require('cors');
// var logger = require('./server/lib/logger');
const host = require('./server/config/host');

require('dotenv').config();

var app = express();
var server = http.createServer(app);
var env = process.env.NODE_ENV;

/*
 * WEBPACK MIDDLEWARE
 */
app.use(session({
	secret: 'somerandonstuffsxxxdsdsd',
	resave: false,
	saveUninitialized: false,
	cookie: {
		expires: 6000000
	}
}));
var config = require('./webpack.config');
var compiler = webpack(config);
app.use(require('webpack-dev-middleware')(compiler, {
	noInfo: true,
	publicPath: config.output.publicPath
}));
if (env === "local") {
	app.use(require('webpack-hot-middleware')(compiler));
}

/*
 * ESSENTIAL MIDDLWARE
 */
app.use('/assets', express.static(__dirname + '/public'));
app.use(cors({ origin: host }));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.disable('x-powered-by');
app.set("views", "./server/views");

/*
 * ROUTE HANDLER SETUP
 */
require('./server/route')(app);
/**
 * PROCESS UNCAUGHT ERROR
 */
// app.use(function (err, req, res, next) {
// 	 logger.error('express catch error ', err.message);
// 	res.status(500).send('error');
// });

server.listen(8001, function () {
	console.log('listening on 8001');
});

module.exports = server;
