const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const path = require('path');
require('dotenv').config();

const env = process.env.NODE_ENV;
var plugins = [
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin({ "process.env": { NODE_ENV: JSON.stringify(env) } }),
  new Dotenv()
];

var entry = {
  'agent': ['./client/Agent/app']
};

if (env === "local") {
  plugins.push(new webpack.HotModuleReplacementPlugin());
  entry['agent'].push('webpack-hot-middleware/client');
} else if (env === "production") {
  plugins.push(new webpack.optimize.UglifyJsPlugin({
    minimize: true,
    compress: {
      warnings: false
    }
  }));
}

module.exports = {
  cache: true,
  devtool: 'eval',
  entry: {
    'agent': entry['agent']
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].bundle.js",
    publicPath: "/static/"
  },
  plugins: plugins,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        include: path.join(__dirname, 'client'),
        query: {
          cacheDirectory: true
        }
      }
    ]
  },
  resolve: {
    alias: {
      gdebug: __dirname + '/client/library/debug.js',
      jwt: __dirname + '/jsonwebtoken/index.js',
    },
    extensions: ['.js']
  },
  node: {
    fs: 'empty'
  }
};